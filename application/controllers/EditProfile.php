<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class EditProfile extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this->load->model('M_EditProfile');
    }
    
    public function index_post() {

        $ProfileData = $this->post('ProfileData');
        $UserID = $this->post('UserID');

        $UserName = $this->post('UserName');
        $UserEmail = $this->post('UserEmail');
        $UserGender = $this->post('UserGender');
        $UserBirth = $this->post('UserBirth');

        if ($ProfileData && $UserID) {
            $image = base64_decode($this->post('ProfileData'));

            $filename = "profile_".time().'.png';
            $path = '../gambar/';

            if(file_put_contents($path.$filename, $image)){
                if ($this->M_EditProfile->UpdatePhotoProfile('gambar/'.$filename, $UserID)) {
                    $this->response([
                        'status' => true,
                        'message' => 'data terupdate',
                        'UserID' => $UserID,
                        'Url' => $path.$filename
                    ], 200);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'Terjadi Kesalahan Saat Mengupdate database!',
                    ], 404);
                }
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Terjadi Kesalahan!',
                    'error' => $this->upload->display_errors()
                ], 404);
            }
        } else if ($UserName && $UserEmail && $UserGender && $UserBirth && $UserID) {
            if ($this->M_EditProfile->UpdateProfile($UserID, $UserName, $UserEmail, $UserGender, $UserBirth)) {
                $this->response([
                    'status' => true,
                    'message' => 'data terupdate',
                    'UserID' => $UserID,
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Terjadi Kesalahan Saat Mengupdate database!',
                ], 404);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }

}