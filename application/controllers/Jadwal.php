<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Jadwal extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this->load->model('M_Jadwal');
    }

    public function index_get() {
        $ClassID = $this->get('ClassID');
        $Day = $this->get('Day');
        if ($ClassID && $Day) {
            $buku = $this->M_Jadwal->getJadwalByClassDate($ClassID, $Day);
            if ($buku) {
                $this->response([
                    'status' => true,
                    'message' => '',
                    'data' => $buku,
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'tidak ada data!'
                ], 404);
            }
            
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }
}