<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Diskusi extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this->load->model('M_Diskusi');
    }
    
    public function index_post() {

        $DiscussionContent = $this->post('DiscussionContent');
        $UserID = $this->post('UserID');

        if ($DiscussionContent && $UserID) {
            $this->M_Diskusi->insertDiskusi($DiscussionContent, $UserID);
            $token = $this->M_Diskusi->getToken($UserID);
            $class = $this->M_Diskusi->getClass($UserID);
            
            $this->load->library('notification');

            $this->notification->sendNotif($token, 'Diskusi Kelas', $class[0]->UserName .' : '.$DiscussionContent);
            $this->response([
                'status' => true,
                'message' => '',
                'token' => $token,
                'isi' => $class[0]->UserName .' : '.$DiscussionContent,
                'kelas' => $class[0]->ClassName,
            ], 200);
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }

    public function index_get() {
        $UserID = $this->get('UserID');
        if ($UserID) {
            $class = $this->M_Diskusi->getClass($UserID);
            if ($class) {

                $IDSiswa = $this->M_Diskusi->getSiswaIDByClass($class[0]->ClassID);
                $IDGuru = $this->M_Diskusi->getGuruIDByClass($class[0]->ClassID);

                $AllUserID = array_merge($IDGuru,$IDSiswa);

                $diskusi = $this->M_Diskusi->getDiskusi($AllUserID);
                $this->response([
                    'status' => true,
                    'message' => '',
                    'data' => $diskusi,
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'tidak ada data!'
                ], 404);
            }
            
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }

}