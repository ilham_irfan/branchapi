<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Login extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this->load->model('M_login');
    }
    
    public function index_post() {

        $UserEmail = $this->post('UserEmail');
        $UserPassword = $this->post('UserPassword');
        $LoginToken = $this->post('LoginToken');
        $UserID = $this->post('UserID');

        if ($UserEmail && $UserPassword && $LoginToken) {
            
            $pengacak = "p3ng4c4k";

            $UserPassword1 = MD5($UserPassword);
            $UserPassword2 = md5($pengacak . md5($UserPassword1));

            $user = $this->M_login->check_login(['user.UserEmail' => $UserEmail, 'user.UserPassword' => $UserPassword2, 'user.isDelete' => 0], $LoginToken);
            if ($user) {
                if ($user[0]->UserLevel == 1) {
                    $this->response([
                        'status' => true,
                        'message' => '',
                        'data' => $user
                    ], 200);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'User bukan siswa!'
                    ], 404);
                }
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'User tidak ditemukan!'
                ], 404);
            }
        } else if($UserEmail) {
            $user = $this->M_login->check_email(['UserEmail' => $UserEmail, 'isDelete' => 0]);
            if ($user) {
                if ($user[0]->UserLevel == 1) {
                    $this->response([
                        'status' => true,
                        'message' => '',
                        'data' => ['UserEmail' =>  $user[0]->UserEmail, 'UserName' =>  $user[0]->UserName]
                    ], 200);
                } else {
                    $this->response([
                        'status' => false,
                        'message' => 'User bukan siswa!'
                    ], 404);
                }
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'User tidak ditemukan!'
                ], 404);
            }
        } else if ($LoginToken && $UserID) {
            $user = $this->M_login->updateToken($UserID, $LoginToken);
            if ($user) {
                $this->response([
                    'status' => true,
                    'message' => '',
                    'data' => 'succes'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'User tidak ditemukan!'
                ], 404);
            }
        }else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }

    public function logout_post() {
        $UserID = $this->post('UserID');

        if ($UserID) {
            $user = $this->M_login->logout($UserID);
            if ($user) {
                $this->response([
                    'status' => true,
                    'message' => '',
                    'data' => 'succes'
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'User tidak ditemukan!'
                ], 404);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }

}