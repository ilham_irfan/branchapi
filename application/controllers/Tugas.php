<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */

//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Tugas extends CI_Controller {

    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this->load->model('M_Tugas');
    }

    public function index_get() {
        $ClassID = $this->get('ClassID');
        if ($ClassID) {
            $tugas = $this->M_Tugas->getAllHomeWorkByClass($ClassID);
            if ($tugas) {
                $this->response([
                    'status' => true,
                    'message' => '',
                    'data' => $tugas,
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'tidak ada data!'
                ], 404);
            }
            
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }

    public function Tugas_get() {
        $HomeWorkID = $this->get('HomeWorkID');
        $UserID = $this->get('UserID');
        if ($HomeWorkID && $UserID) {
            $tugas = $this->M_Tugas->getHomeWorkByID($HomeWorkID);
            $pertanyaan = $this->M_Tugas->getHomeWorkQuestionByID($HomeWorkID);
            $jawaban = array();
            foreach ($pertanyaan as $index=>$question) {
                $myJawab = $this->M_Tugas->getHomeWorkAnswerByID($question->HomeWorkQuestionID);
                foreach ($myJawab as &$jawab) {
                    $jawab->index = $index;
                }
                $UserAnswer = $this->M_Tugas->getHomeWorkUserAnswer($UserID, $question->HomeWorkQuestionID);
                if ($UserAnswer) {
                    $question->UserAnswer = $UserAnswer[0]->HomeWorkUserAnswerValue;
                } else {
                    $question->UserAnswer = null;
                }
                
                array_push($jawaban ,$myJawab);
            }
            if ($tugas) {
                $this->response([
                    'status' => true,
                    'message' => '',
                    'data' => [
                        'detail' => $tugas,
                        'soal' => $pertanyaan,
                        'jawaban' => $jawaban
                    ],
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'tidak ada data!'
                ], 404);
            }
            
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }

    public function SendAnswer_post() {
        $HomeWorkID = $this->post('HomeWorkID');
        $UserID = $this->post('UserID');
        $HomeWorkQuestionID = $this->post('HomeWorkQuestionID');
        $HomeWorkUserAnswerValue = $this->post('Value');

        if ($HomeWorkID && $UserID && $HomeWorkUserAnswerValue && $HomeWorkQuestionID) {
            $answer = $this->M_Tugas->addUserAnswer($HomeWorkID, $UserID, $HomeWorkQuestionID, $HomeWorkUserAnswerValue);
            if ($answer) {
                $this->response([
                    'status' => true,
                    'message' => 'berhasil',
                ], 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'terjadi kesalahan'
                ], 404);
            }
        } else {
            $this->response([
                'status' => false,
                'message' => 'Kesalahan parameter!'
            ], 404);
        }
    }
}