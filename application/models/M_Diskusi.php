<?php  
class M_Diskusi extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
    }
        
    function insertDiskusi($DiscussionContent, $UserID){
        $Discussion = array(
            'DiscussionContent' =>$DiscussionContent,
            'UserID' => $UserID,
            'createdAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        $this->db->insert('discussion', $Discussion);
    }

    function getToken($UserID){
        $this->db->join('userstudent', 'userstudent.UserStudentID = user.UserDetailID', 'CONCAT');
        $user = $this->db->get_where('user', ['user.UserID' => $UserID, 'user.isDelete' => 0, 'user.UserLevel' => 1])->result();

        $this->db->join('user', 'userstudent.UserStudentID = user.UserDetailID', 'CONCAT');
        $user2 = $this->db->get_where('userstudent', ['userstudent.ClassID' => $user[0]->ClassID, 'userstudent.isDelete' => 0, 'user.UserLevel' => 1])->result();
        return array_column($user2, 'LoginToken');
    }

    function getClass($UserID){
        $this->db->join('userstudent', 'userstudent.UserStudentID = user.UserDetailID', 'CONCAT');
        $this->db->join('class', 'class.ClassID = userstudent.ClassID', 'CONCAT');
        return $this->db->get_where('user', ['user.UserID' => $UserID, 'user.isDelete' => 0, 'user.UserLevel' => 1])->result();
    }

    function getDiskusi($UserID){
        $this->db->select('discussion.*, user.UserName, user.UserLevel, user.UserGender');
        $this->db->join('user', 'user.UserID = discussion.UserID', 'CONCAT');
        $this->db->join('userstudent', 'userstudent.UserStudentID = user.UserDetailID', 'CONCAT');
        $this->db->order_by("discussion.createdAt", "DESC");
        $this->db->where_in('user.UserID', $UserID);
        return $this->db->get_where('discussion', ['discussion.isDelete' => 0])->result();
    }

    public function getSiswaIDByClass($ClassID)
    {
        $this->db->select('*');
        $this->db->join('user', 'user.UserID = userstudent.UserStudentID', 'CONCAT');
        $query = $this->db->get_where('userstudent', ["userstudent.isDelete" => 0, "userstudent.ClassID" => $ClassID])->result();
        return array_unique(array_column($query, 'UserID'));
    }

    public function getGuruIDByClass($ClassID)
    {
        $this->db->select('*');
        $query = $this->db->get_where('schoolschedule', ["schoolschedule.isDelete" => 0, "schoolschedule.ClassID" => $ClassID])->result();
        return array_unique(array_column($query, 'UserID'));
    }
}
?>