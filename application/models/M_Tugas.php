<?php  
class M_Tugas extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getAllHomeWorkByClass($CLassID) {
        $this->db->join('class', "class.ClassID = homework.ClassID", 'CONCAT');
        $this->db->join('subjectschool', "subjectschool.SubjectSchoolID = homework.SubjectID", 'CONCAT');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'CONCAT');
        $this->db->where('homework.HomeWorkDate >=', date("Y-m-d H:i:s"));
        $query = $this->db->get_where('homework', ["homework.isDelete" => 0, 'homework.ClassID' => $CLassID]);
        return $query->result();
    }

    public function getHomeWorkByID($ID) {
        $this->db->join('class', "class.ClassID = homework.ClassID", 'CONCAT');
        $this->db->join('subjectschool', "subjectschool.SubjectSchoolID = homework.SubjectID", 'CONCAT');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'CONCAT');
        $query = $this->db->get_where('homework', ["homework.isDelete" => 0, 'homework.HomeWorkID' => $ID]);
        return $query->result();
    }

    public function getHomeWorkQuestionByID($ID) {
        $query = $this->db->get_where('homeworkquestion', ["isDelete" => 0, 'HomeWorkID' => $ID]);
        return $query->result();
    }

    public function getHomeWorkUserAnswer($UserID, $QuestionID) {
        $query = $this->db->get_where('homeworkuseranswer', ["isDelete" => 0, 'UserID' => $UserID, 'HomeWorkQuestionID' => $QuestionID]);
        return $query->result();
    }

    public function getHomeWorkAnswerByID($ID) {
        $query = $this->db->get_where('homeworkanswer', ["isDelete" => 0, 'HomeWorkQuestionID' => $ID]);
        return $query->result();
    }

    function addUserAnswer($HomeWorkID, $UserID, $HomeWorkQuestionID, $HomeWorkUserAnswerValue){

        $data = $this->db->get_where('homeworkuseranswer', ["isDelete" => 0, 'UserID' => $UserID, 'HomeWorkQuestionID' => $HomeWorkQuestionID])->result();

        if (!$data) {
            $Answer = array(
                'HomeWorkUserAnswerValue' =>$HomeWorkUserAnswerValue,
                'UserID' => $UserID,
                'HomeWorkQuestionID' => $HomeWorkQuestionID,
                'HomeWorkID' => $HomeWorkID,
                'createdAt' => date("y-m-d H:i:s"),
                'isDelete' => 0,
            );
            return $this->db->insert('homeworkuseranswer', $Answer);
        } else {
            $Answer = array(
                'HomeWorkUserAnswerValue' =>$HomeWorkUserAnswerValue,
                'updateAt' => date("y-m-d H:i:s"),
            );
            $this->db->where(["isDelete" => 0, 'UserID' => $UserID, 'HomeWorkQuestionID' => $HomeWorkQuestionID]);
            return $this->db->update('homeworkuseranswer', $Answer);
        }

        
    }
}
?>