<?php  
class M_Jadwal extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getJadwalByClassDate($ClassID, $day)
    {
        $this->db->join('user', "user.UserID = schoolschedule.UserID", 'CONCAT');
        $this->db->join('subjectschool', "subjectschool.SubjectSchoolID = schoolschedule.SubjectSchoolID", 'CONCAT');
        $this->db->join('subject', "subject.SubjectID = subjectschool.SubjectID", 'LEFT OUTER');
        $this->db->order_by("schoolschedule.SchoolScheduleHour", "asc");
        $query = $this->db->get_where('schoolschedule', ["schoolschedule.isDelete" => 0, 'schoolschedule.ClassID' => $ClassID, 'schoolschedule.SchoolScheduleDay' => $day]);
        return $query->result();
    }
}
?>