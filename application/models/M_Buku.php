<?php  
class M_Buku extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getAllBukuByClass($CLassID) {
        $this->db->join('subject', "subject.SubjectID = book.SubjectID", 'CONCAT');
        $query = $this->db->get_where('book', ["book.isDelete" => 0, 'book.ClassID' => $CLassID]);
        return $query->result();
    }
}
?>