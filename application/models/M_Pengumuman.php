<?php  
class M_Pengumuman extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
    }

    public function getAllAnnoucementByClass($CLassID) {
        $this->db->select('schoolannoucement.*');
        $this->db->join('class', 'schoolannoucement.SchoolID = class.SchoolID', 'CONCAT');
        $this->db->order_by('schoolannoucement.createdAt', 'DESC');
        $this->db->where('schoolannoucement.createdAt >=', date('Y-m-d H:i:s',strtotime('-5 day',strtotime(date("Y-m-d H:i:s")))));
        $query = $this->db->get_where('schoolannoucement', ["schoolannoucement.isDelete" => 0, 'class.ClassID' => $CLassID]);
        return $query->result();
    }
}
?>