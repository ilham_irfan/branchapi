<?php  
class M_login extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
    }
        
    function check_login($data, $token){
        $user = $this->db->get_where('user', $data);

        if ($user->num_rows() > 0) {
            $UserData = $user->result();
            $this->db->update('user', array('LoginToken' => $token, 'LastLogin' => date("y-m-d H:i:s")), array('UserID' => $UserData[0]->UserID));
            
            $this->db->join('userstudent', 'userstudent.UserStudentID = user.UserDetailID', 'CONCAT');
            $this->db->join('class', 'userstudent.ClassID = class.ClassID', 'CONCAT');
            $this->db->join('school', 'class.SchoolID = school.SchoolID', 'CONCAT');
            return $this->db->get_where('user', $data)->result();
        } else {
            return FALSE;
        }
    }

    function check_email($data){
        return $this->db->get_where('user', $data)->result();
    }

    function updateToken($UserID, $token) {    
        return $this->db->update('user', array('LoginToken' => $token, 'LastOpenApp' => date("y-m-d H:i:s")), array('UserID' => $UserID));
    }

    function logout($UserID) {    
        return $this->db->update('user', array('LoginToken' => '', 'LastOpenApp' => date("y-m-d H:i:s"), 'LastLogin' => date("y-m-d H:i:s")), array('UserID' => $UserID));
    }
}
?>