<?php  
class M_EditProfile extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
    }
        
    function UpdatePhotoProfile($Url, $UserID){
        $Profile = array(
            'UserProfile' => $Url,
            'updateAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        $this->db->where("UserID = '$UserID'");
        return $this->db->update('user', $Profile);
    }

    function UpdateProfile($UserID, $UserName, $UserEmail, $UserGender, $UserBirth){
        $Profile = array(
            'UserName' => $UserName,
            'UserEmail' => $UserEmail,
            'UserGender' => $UserGender,
            'UserBirth' => $UserBirth,
            'updateAt' => date("y-m-d H:i:s"),
            'isDelete' => 0,
        );
        $this->db->where("UserID = '$UserID'");
        return $this->db->update('user', $Profile);
    }
}
?>